package minh.nguyen.eas.air.ticket.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchController {

	@RequestMapping("/search")
	public String search() {
		return "search";
	}
	
	@RequestMapping("/search/{0}")
	public String searchDetails() {
		return "search details";
	}
}

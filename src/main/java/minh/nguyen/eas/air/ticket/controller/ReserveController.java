package minh.nguyen.eas.air.ticket.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReserveController {

	@RequestMapping("/reserve")
	public String reserve() {
		return "reserve";
	}
}
